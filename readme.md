# tfeiszt ResumeTemplate  

### Description
Responsive resume html template based on Bootstrap 3.
 
### Browser compatibility
   * IE9
   * IE10
   * IE11
   * Firefox
   * Safari
   * Opera
   * Chrome
   * Edge

### Pre-processors
  * Sass
  
### Compiler
  * Grunt
  
### Installing  

```
$ npm install
$ grunt
```
  
### Grunt setting to build production
See option "devMode" in package.json. If this option is false then css and js files are minimized, otherwise not. Default option is true.
```
{
    "name": "resume-template",
    "descrition": "Resume template for personal websites",
    "version": "0.0.1",
    "devMode": false,
```

### Screenshots
[Screen](screenshots/screen.png)

[Tablet](screenshots/tablet.png)

[Mobile](screenshots/mobile.png)
 
### License

MIT
