module.exports = function (grunt) {
    var package = grunt.file.readJSON('package.json');

    grunt.initConfig({

        // Minify JS
        uglify: {
            main: {
                options: {
                    beautify: package.devMode
                },
                files: [
                    {
                        expand: true,
                        cwd: 'assets/js/',
                        src: '*.js',
                        dest: 'assets/js/dist/',
                        rename: function (dest, matchedSrcPath, options) {
                            // return the destination path and filename:
                            return (dest + matchedSrcPath).replace('.js', '.min.js');
                        }
                    }
                ]
            }
        },

        // Compile SCSS into CSS
        sass: {
            main: {
                files: {
                    'assets/css/main.css': 'assets/css/main.scss'
                }
            }
        },

        // Clean map files
        clean: {
            scss: [
                'assets/css/main.css.map'
            ],
            css: [
                'assets/css/main.css'
            ]
        },

        // Minify CSS
        cssmin: {
            main: {
                options: {
                    shorthandCompacting: false,
                    roundingPrecision: -1
                },
                files: [
                    {
                        expand: true,
                        cwd: 'assets/css/',
                        src: '*.css',
                        dest: 'assets/css/dist/',
                        rename: function (dest, matchedSrcPath, options) {
                            // return the destination path and filename:
                            return (dest + matchedSrcPath).replace('.css', '.min.css');
                        }
                    }
                ]
            }
        },

        // No minify on css file
        copy: {
            main: {
                src: 'assets/css/main.css',
                dest: 'assets/css/dist/main.min.css'
            },
        },

        // watch changes
        watch: {
            js: {
                files: [
                    'assets/js/*.js'
                ],
                tasks: ['uglify']
            },

            css: {
                files: [
                    'assets/css/*.scss'
                ],
                tasks: ['sass:main', ((package.devMode === true) ? 'copy:main' : 'cssmin:main'), 'clean:scss', 'clean:css']
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');

    if (package.devMode === true) {
        grunt.registerTask('default', ['uglify', 'sass:main', 'copy', 'clean:scss', 'clean:css', 'watch']);
    } else {
        grunt.registerTask('default', ['uglify', 'sass:main', 'cssmin', 'clean:scss', 'clean:css']);
    }
};