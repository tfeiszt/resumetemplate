/**
 * Copy timeline item details into modal area.
 * @param sender - Sender button instance
 */
function showDetails(sender) {
    if (sender && typeof sender !== 'undefined') {
        $('#project-modal').on('show.bs.modal', function () {
            //Show local loader
            $('#project-modal .loader').fadeIn();

            //Copy content
            var anchor = '<a href="http://' + sender.closest('div').find('.hidden').attr('data-link') + '" target="_blank">' + sender.closest('div').find('.hidden').attr('data-link') + '</a>';
            $('#project-modal .left-col').html(sender.closest('div').find('.hidden .left-col').html());
            $('#project-modal .right-col').html(sender.closest('div').find('.hidden .right-col').html());
            $('#project-modal p.website').html(anchor);

            //It's empty for now, but can be useful if you want to initialize a slider on the modal layer.
            setTimeout(function () {
                $('#project-modal .loader').fadeOut();
            }, 500);

        }).modal();
    }
}

/**
 * window.load
 */
$(window).load(function () {
    //hide loader
    $('#page-loader').fadeOut(500);
});

/**
 * document.ready
 */
$(document).ready(function() {
    //Menu open-close
    $('#menuToggle, .menu-close').on('click', function () {
        $('#menuToggle').toggleClass('active');
        $('body').toggleClass('body-push-toleft');
        $('#theMenu').toggleClass('menu-open');
    });
    //Read more click events
    $('a.cd-read-more').on('click', function() {
        showDetails($(this));
        return false;
    });
    //scrolling
    $('#theMenu .smoothScroll').on('click', function(e) {
        e.preventDefault();
        $('#menuToggle').click();
    });
});
